// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */

class ATank;
class UTankAimingComponent;

UCLASS()
class BATTLETANKS_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
		 


private:
	void AimTowardsCrosshair();
	bool GetSightRayHitLocation(FVector &OutHitLoc);
	void BeginPlay() override;
	void Tick(float DeltaTime) override;
	FVector GetLookVectorHitLocation(FVector Location, FVector Rotation);

private:
	UPROPERTY(EditAnywhere)
	float CrossHairXLocation = .5f; 
	
	UPROPERTY(EditAnywhere)
	float CrossHairYLocation = .3333333f;

	UPROPERTY(EditAnywhere)
	float RayLength = 10000;
	
	UTankAimingComponent* AimingComponent = nullptr;

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void FoundAimingComponent(UTankAimingComponent* AimComponentRef);

	void SetPawn(APawn* InPawn) override;

private:
	UFUNCTION()
		void OnPossessedTankDeath();
};
