// Fill out your copyright notice in the Description page of Project Settings.



#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "Tank.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"



void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	AimingComponent = GetPawn()->FindComponentByClass < UTankAimingComponent>();
	if (AimingComponent)
		FoundAimingComponent(AimingComponent);
	else 
		UE_LOG(LogTemp, Warning, TEXT("Player controller cant find aiming component at Begin Play."))

}

void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();
}

void ATankPlayerController::AimTowardsCrosshair()
{
	if (!ensure(GetPawn()))
		return;
	
	// Raycasting
	FVector HitLocation;  // out parameter

	// Get world location if linetrace through crosshair
	// If it hits landscape
		// Tell controlled tank to aim at this point
	if (GetSightRayHitLocation(HitLocation))
	{
		AimingComponent->AimAt(HitLocation);
		//UE_LOG(LogTemp, Warning, TEXT("Hit Location: %s"), *HitLocation.ToString())
	}

}

bool ATankPlayerController::GetSightRayHitLocation(FVector &OutHitLoc)
{
	int32 ViewPortSizeX, ViewPortSizeY;
	GetViewportSize(ViewPortSizeX, ViewPortSizeY);

	auto ScreenLocation = FVector2D(ViewPortSizeX * CrossHairXLocation, ViewPortSizeY * CrossHairYLocation);

	FVector LookDirection;
	FVector LookLocation;

	if(DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, LookLocation, LookDirection))
	{
		OutHitLoc = GetLookVectorHitLocation(LookLocation, LookDirection);

		//GetPawn()->AimAt(OutHitLoc);
		return true;
	}
	return false;

}

FVector ATankPlayerController::GetLookVectorHitLocation(FVector LookLocation, FVector LookDirection)
{
	FHitResult Hit;
	FCollisionQueryParams collision(NAME_None, false, GetPawn());

	GetWorld()->LineTraceSingleByChannel(Hit, LookLocation,
		LookLocation + LookDirection * RayLength,
		ECollisionChannel::ECC_Visibility,
		collision);
	return Hit.Location;
}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }

		// Subscribe our local method to the tank's death event
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnPossessedTankDeath);
	}
}

void ATankPlayerController::OnPossessedTankDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("HERE"))
}















