// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

class UTankBarrel;
class UTankTurret;
class AProjectile;

UENUM()
enum class EFiringState : uint8 {
	Reloading,
	Aiming,
	Locked

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BATTLETANKS_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

	

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();
	UFUNCTION(BlueprintCallable)
	void Initialize(UTankBarrel* Barrel, UTankTurret* Turret);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(BlueprintReadOnly, Category = "State")
		EFiringState FiringState = EFiringState::Aiming;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void AimAt(FVector HitLocation);
	UFUNCTION(BlueprintCallable, Category = Projectile)
		void Fire();
	EFiringState GetFiringState() const;
		
private: 
	UTankBarrel* Barrel = nullptr;
	UTankTurret* Turret = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = Shooting)
		float LaunchSpeed = 100000000;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
		TSubclassOf<AProjectile> ProjectileBlueprint;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float ReloadTimeSecs = 3;

	double LastFireTime = -3;
	FVector AimDirection = FVector(0, 0, 0);

	
private:
	//bool SuggestProjectileVelocity(FVector& OutVelocity, FVector StartPosition, FVector EndPosition, float LaunchVelocity, float Gravity = 9.81f, bool IsGetUpperTrajectory = false);
	void MoveBarrel(FVector AimDirection);
	void MoveTurret(FVector AimDirection);
	bool IsBarrelMoving();
};
