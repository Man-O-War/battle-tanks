// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANKS_API ATankAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;
	void Tick(float DeltaTime) override;

protected:

	// How close the enemy tank can get
	UPROPERTY(EditDefaultsOnly)
		float AcceptanceRadius = 80000;
	
	virtual void SetPawn(APawn* InPawn) override;

private:
	UFUNCTION()
		void OnPossessedTankDeath();
};
