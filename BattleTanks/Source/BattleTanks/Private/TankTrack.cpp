// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTrack.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"


void UTankTrack::DriveTank()
{
	
}

void UTankTrack::SetThrottle(float Throttle)
{ 
	// Line trace to look for earth
	FHitResult Hit;
	FCollisionQueryParams collision(NAME_None, false, GetOwner());


	if (GetWorld()->LineTraceSingleByChannel(Hit, GetComponentLocation(),
		GetComponentLocation() + -GetUpVector().GetSafeNormal() * LineTraceLength,
		ECollisionChannel::ECC_WorldStatic, collision))
	{
		// Calculate Force
		FVector ForceApplied = GetForwardVector() * Throttle * TrackMaxDrivingForce;
		FVector ForceLocation = GetComponentLocation();

		auto TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());

		// Add force
		TankRoot->AddForceAtLocation(ForceApplied, ForceLocation);
		
		// Apply corrections
		ApplySidewaysForce();
	}
}

void UTankTrack::ApplySidewaysForce()
{
	// Work-out the required acceleration this frame to correct
	auto SlippageSpeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
	auto DeltaTime = GetWorld()->GetDeltaSeconds();
	auto CorrectionAcceleration = -SlippageSpeed / DeltaTime * GetRightVector();

	// Calculate and apply sideways (F = m a)
	auto TankRoot = Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
	auto CorrectionForce = (TankRoot->GetMass() * CorrectionAcceleration * .05) / 2; // Two tracks
	TankRoot->AddForce(CorrectionForce);
}


void UTankTrack::BeginPlay() {
	OnComponentHit.AddDynamic(this, &UTankTrack::OnHit);
}

void UTankTrack::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	CurrentThrottle = 0;
	

}



