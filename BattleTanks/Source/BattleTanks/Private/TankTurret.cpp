// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTurret.h"
#include "Engine/World.h"


void UTankTurret::Rotate(float Value) {
	//UE_LOG(LogTemp, Warning, TEXT("%f"), Value)
	float DeltaRotation = FMath::Clamp(Value, -1.f, 1.f) * GetWorld()->GetDeltaSeconds() * MaxRotationDegrees;

	FRotator RotationValue = (RelativeRotation + FRotator(0, DeltaRotation, 0));

	SetRelativeRotation(RotationValue);
}

