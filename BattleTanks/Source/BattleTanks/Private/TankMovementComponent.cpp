// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"
#include "TankTrack.h"

void UTankMovementComponent::IntendMoveForward(float Throw)
{
	if (LeftTrack && RightTrack)
	{
		LeftTrack->SetThrottle(Throw);
		RightTrack->SetThrottle(Throw);
	}
}

void UTankMovementComponent::Initialize(UTankTrack* LeftTrack, UTankTrack* RightTrack)
{
	this->LeftTrack = LeftTrack;
	this->RightTrack = RightTrack;
}

void UTankMovementComponent::IntendRotate(float RightwardRotation)
{
	if (LeftTrack && RightTrack)
	{
		LeftTrack->SetThrottle(-RightwardRotation);
		RightTrack->SetThrottle(RightwardRotation);
	}
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	

	FVector AIForwardIntention = MoveVelocity.GetSafeNormal();
	FVector TankIntention = GetOwner()->GetActorForwardVector().GetSafeNormal();

	float Direction = FVector::DotProduct(AIForwardIntention, TankIntention);
	FVector CrossProduct = FVector::CrossProduct(AIForwardIntention, TankIntention);
	
	UE_LOG(LogTemp, Warning, TEXT("%s moving at %f"), *GetOwner()->GetName(), Direction)

	IntendMoveForward(Direction);
	IntendRotate(CrossProduct.Z);

}

