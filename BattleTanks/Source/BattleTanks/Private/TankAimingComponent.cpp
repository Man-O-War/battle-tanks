// Fill out your copyright notice in the Description page of Project Settings.



#include "TankAimingComponent.h"
#include "Projectile.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "TankBarrel.h"
#include "TankTurret.h"


// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


void UTankAimingComponent::Initialize(UTankBarrel* Barrel, UTankTurret* Turret)
{
	this->Barrel = Barrel; 
	this->Turret = Turret;
}


// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...

	if (GetWorld()->GetTimeSeconds() - LastFireTime < ReloadTimeSecs){
		FiringState = EFiringState::Reloading;
		//UE_LOG(LogTemp, Warning, TEXT("Reloading"))
	}
	else if (IsBarrelMoving())
	{
		FiringState = EFiringState::Locked;
		//UE_LOG(LogTemp, Warning, TEXT("Aiming"))
	}
	else
	{
		FiringState = EFiringState::Aiming;
		//UE_LOG(LogTemp, Warning, TEXT("Locked"))
	}

}


void UTankAimingComponent::AimAt(FVector AimLocation)
{
	FVector OutLaunchVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("Shoot"));

	if (UGameplayStatics::SuggestProjectileVelocity(
		this,
		OutLaunchVelocity,
		StartLocation,
		AimLocation,
		LaunchSpeed,
		false,
		0,
		0,
		ESuggestProjVelocityTraceOption::DoNotTrace
	))
	{
		AimDirection = OutLaunchVelocity.GetSafeNormal();
		if (AimDirection != FVector(0,0,0))
			MoveBarrel(AimDirection);		
	}




	if (Turret && AimLocation != FVector(0,0,0))
		MoveTurret(AimLocation);
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensure(Barrel)) { return false; }
	auto BarrelForward = Barrel->GetForwardVector();
	return !BarrelForward.Equals(AimDirection, 0.01); // vectors are equal
}


void UTankAimingComponent::MoveBarrel(FVector AimDirection)
{
	FRotator AimRotation = AimDirection.Rotation();
	FRotator TurretRotation = Barrel->GetForwardVector().Rotation();

	FRotator DeltaRotation = AimRotation - TurretRotation;

	Barrel->Elevate(DeltaRotation.Pitch);
}

void UTankAimingComponent::MoveTurret(FVector AimLocation) {
	FVector DifferenceInVectors = AimLocation - Turret->GetComponentLocation();
	FRotator DirectionalVector = DifferenceInVectors.Rotation();
	
	FRotator DeltaRotation = Barrel->GetForwardVector().Rotation() - DirectionalVector;
	if (FMath::Abs(DeltaRotation.Yaw) > 180)
		Turret->Rotate(DeltaRotation.Yaw);
	else
		Turret->Rotate(-DeltaRotation.Yaw);
	
}

void UTankAimingComponent::Fire()
{
	if (FiringState == EFiringState::Reloading) { return; }
	UE_LOG(LogTemp, Warning, TEXT("FIRING"))
	if (!ensure(Barrel)) { return; }

	auto Projectile = GetWorld()->SpawnActor<AProjectile>(
		ProjectileBlueprint,
		Barrel->GetSocketLocation(FName("Shoot")),
		Barrel->GetSocketRotation(FName("Shoot"))
	);

	Projectile->LaunchProjectile(LaunchSpeed);
	if (GetWorld())
		LastFireTime = GetWorld()->GetTimeSeconds();
}

EFiringState UTankAimingComponent::GetFiringState() const
{
	return FiringState;
}

















//bool UTankAimingComponent::SuggestProjectileVelocity(FVector& OutVelocity, FVector StartPosition, FVector EndPosition, double LaunchVelocity, double Gravity, bool IsGetUpperTrajectory) {
//	double Dv = EndPosition.Z - StartPosition.Z; // Vertical change 
//
//											//Inter-variables
//	double XDiffAndSquare = FMath::Pow(EndPosition.X - StartPosition.X, 2);
//	double YDiffAndSquare = FMath::Pow(EndPosition.Y - StartPosition.Y, 2);
//
//
//	double XSRatio = (XDiffAndSquare + YDiffAndSquare) / XDiffAndSquare; // s/x 
//
//
//	double Dh = FMath::Sqrt(XDiffAndSquare + YDiffAndSquare); // Horizontal change //TODO Check compatibility
//
//
//	double Discriminant = 1 - FMath::Pow(Gravity * Dh * FMath::Pow(1 / LaunchVelocity, 2), 2) - 2 * Dv * Gravity / FMath::Pow(LaunchVelocity, 2);
//	
//	if (Discriminant < 0){
//		OutVelocity = FVector(0,0,0);
//		return false;
//	}
//
//	int SignChange = IsGetUpperTrajectory ? -1 : 1;
//	double Angle = FMath::Atan((1 - (SignChange)* FMath::Sqrt(Discriminant)) / (Gravity * Dh / FMath::Pow(LaunchVelocity, 2)));
//
//    // Transform into FVector2D
//	FVector2D Rep2D = FVector2D(FMath::Cos(Angle), FMath::Sin(Angle));
//
//	// Transform into 3D using ratios
//	FVector Rep3D = FVector(Rep2D.X / XSRatio, Rep2D.X - Rep2D.X / XSRatio, Rep2D.Y); // TODO Fix ratio
//	OutVelocity = Rep3D * LaunchVelocity;
//	UE_LOG(LogTemp, Warning, TEXT("Dh - %f, Dv - %f, Disc - %f, Angle - %f, Gravity - %f"), Dh, Dv, Discriminant, Angle, Gravity)
//	return true;
//}



