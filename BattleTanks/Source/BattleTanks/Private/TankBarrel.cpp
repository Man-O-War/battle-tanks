// Fill out your copyright notice in the Description page of Project Settings.

#include "TankBarrel.h"
#include "Engine/World.h"

void UTankBarrel::Elevate(float RelativeDegrees)
{
	//UE_LOG(LogTemp, Warning, TEXT("CALLED + %f"), GetWorld()->GetTimeSeconds())
	//UE_LOG(LogTemp, Warning, TEXT("Elevating at %f | Time: %f"), RelativeDegrees * MaxDegreesPerSecond * GetWorld()->GetDeltaSeconds(), GetWorld()->GetTimeSeconds());

	float ElevationChange = FMath::Clamp<float>(RelativeDegrees, -1.f, 1.f) * MaxDegreesPerSecond * GetWorld()->GetDeltaSeconds();

	float RawNewElevation = RelativeRotation.Pitch + ElevationChange;

	
	SetRelativeRotation(FRotator(FMath::Clamp(RawNewElevation, MinElevation, MaxElevation), 0, 0));
}





