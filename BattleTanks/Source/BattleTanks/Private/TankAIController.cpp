// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAIController.h"
#include "Engine/World.h"
#include "Tank.h"
#include "GameFramework/Pawn.h"
#include "TankAimingComponent.h"
#include "GameFramework/PlayerController.h"

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}


void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	APawn *PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	APawn *ControlledTank = GetPawn();

	if (PlayerTank && ControlledTank)
	{
		UTankAimingComponent* AimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
		// Aim towards actor
		AimingComponent->AimAt(PlayerTank->GetTargetLocation());	

		// Move towards actor
		//UE_LOG(LogTemp, Warning, TEXT("%f"), AcceptanceRadius);
		//MoveToActor(PlayerTank, AcceptanceRadius);

		if (AimingComponent->GetFiringState() == EFiringState::Locked)
			AimingComponent->Fire();
	}
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)

	{

		auto PossessedTank = Cast<ATank>(InPawn);

		if (!ensure(PossessedTank)) { return; }



		// Subscribe our local method to the tank's death event

		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossessedTankDeath);

	}
}

void ATankAIController::OnPossessedTankDeath()
{
	GetPawn()->DetachFromControllerPendingDestroy();
}

