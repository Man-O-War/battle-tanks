// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "Projectile.h"


// Sets default values
ATank::ATank()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();

}


float ATank::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser)
{
	CurrentHealth -= FMath::Clamp(DamageAmount, 0.f, CurrentHealth);

	UE_LOG(LogTemp, Warning, TEXT("Current health: %f"), CurrentHealth)


	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
	}
		return CurrentHealth;

}

float ATank::GetHealthPercent() const
{
	return CurrentHealth/100.f; // TODO convert to a variable
}






